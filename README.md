# Jeu de Cartes

## Présentation du projet
Ce projet est un mini-projet de jeu de cartes réalisé dans le cadre d'un entretien technique. Il s'agit d'une application multi-couches utilisant Java 17, Spring Boot version 3 et Maven pour la gestion et la construction du projet.

## Architecture de l'application
L'application suit une architecture multi-couches avec les composants suivants :
- Modèle
- Service
- Repository
- Controller
- Utils
- Configuration
- Présentation

## Technologies utilisées
L'application utilise les technologies suivantes :
- Base de données H2 en mémoire
- JPA Spring Data
- Lombok
- Thymeleaf
- Aspect pour gérer la journalisation des exécutions de méthodes

## Comment lancer le projet
Pour lancer le projet, suivez les étapes suivantes :
1. Cloner le projet
2. Éditer la configuration (ajouter Maven et SDK)
3. Builder avec la commande : `mvn clean install`
4. Lancer le projet

## Comment utiliser l'application
Pour visualiser la page, tapez `url:port//afficher-cartes` dans votre navigateur. Un écran s'affichera avec deux boutons pour jouer et ordonner les cartes.
