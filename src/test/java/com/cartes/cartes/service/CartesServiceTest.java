package com.cartes.cartes.service;

import com.cartes.cartes.model.Cartes;
import com.cartes.cartes.repository.CartesRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

/**
 * Classe de test pour le service CartesService.
 */
@SpringBootTest
public class CartesServiceTest {

    /**
     * Captor pour capturer la liste de cartes passée à la méthode saveAll.
     */
    @Captor
    ArgumentCaptor<List<Cartes>> cartesCaptor;

    /**
     * Instance du service à tester.
     */
    @InjectMocks
    private CartesService cartesService;

    /**
     * Mock du dépôt utilisé par le service.
     */
    @Mock
    private CartesRepository carteRepository;

    /**
     * Test de la méthode saveCartes.
     * Ce test vérifie que la méthode saveAll du dépôt est appelée avec la bonne liste de cartes.
     */
    @Test
    public void testSaveCartes() {
        // Préparez les données de test
        Cartes carte1 = new Cartes("valeur1", "couleur1");
        Cartes carte2 = new Cartes("valeur2", "couleur2");
        List<Cartes> expectedCartes = Arrays.asList(carte1, carte2);

        // Appeler la méthode à tester
        cartesService.saveCartes(expectedCartes);

        // Vérifiez que la méthode saveAll a été appelée avec les bonnes cartes
        verify(carteRepository).saveAll(cartesCaptor.capture());
        List<Cartes> actualCartes = cartesCaptor.getValue();

        // Vérifiez le résultat
        assertEquals(expectedCartes, actualCartes, "Les cartes sauvegardées doivent être les mêmes que celles attendues");
    }

}
