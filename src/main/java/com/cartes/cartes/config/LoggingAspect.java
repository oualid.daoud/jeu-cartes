package com.cartes.cartes.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;


/**
 * Aspect pour gérer la journalisation des exécutions de méthodes.
 */
@Aspect
@Component
public class LoggingAspect {

    /**
     * Logger pour enregistrer les informations.
     */
    private static final Logger logger = LogManager.getLogger(LoggingAspect.class);

    /**
     * Méthode pour journaliser l'exécution des méthodes.
     *
     * @param joinPoint le point de jonction représentant une méthode exécutée.
     * @return le résultat de l'exécution de la méthode.
     * @throws Throwable si une exception est levée lors de l'exécution de la méthode.
     */
    @Around("execution(* com.cartes..*(..)))")
    public Object logMethodExecution(ProceedingJoinPoint joinPoint) throws Throwable {

        // Enregistrement du temps de début
        long start = System.currentTimeMillis();

        // Exécution de la méthode
        Object result = joinPoint.proceed();

        // Calcul du temps écoulé
        long elapsedTime = System.currentTimeMillis() - start;

        // Journalisation de l'information
        logger.info(joinPoint.getSignature() + " exécuté en " + elapsedTime + " millisecondes.");

        return result;
    }
}