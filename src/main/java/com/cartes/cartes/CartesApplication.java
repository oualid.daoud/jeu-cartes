package com.cartes.cartes;

import com.cartes.cartes.model.CarteConsole;
import com.cartes.cartes.model.Cartes;
import com.cartes.cartes.service.CartesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;

/**
 * Classe principale pour l'application de cartes.
 */
@SpringBootApplication
public class CartesApplication {

    /**
     * Service pour gérer les opérations liées aux cartes.
     */
    @Autowired
    private CartesService cartesService;

    /**
     * Méthode principale pour démarrer l'application.
     *
     * @param args les arguments de la ligne de commande.
     */
    public static void main(String[] args) {

        // Création d'une nouvelle console de cartes
        CarteConsole carteConsole = new CarteConsole();

        // Affichage des cartes dans la console
        carteConsole.afficherCartes();

        // Démarrage de l'application Spring
        ConfigurableApplicationContext context = SpringApplication.run(CartesApplication.class, args);

        // Récupération du service de cartes
        CartesService cartesService = context.getBean(CartesService.class);

        // Récupération des cartes de la console
        List<Cartes> cartes = carteConsole.getCartes();

        // Enregistrement des cartes dans le service
        cartesService.saveCartes(cartes);

    }

}
