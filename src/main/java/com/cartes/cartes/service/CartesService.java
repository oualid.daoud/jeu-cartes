package com.cartes.cartes.service;


import com.cartes.cartes.model.Cartes;
import com.cartes.cartes.repository.CartesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

/**
 * Classe de service pour la gestion des cartes.
 * Cette classe fournit des méthodes pour récupérer et réordonner les cartes.
 * Elle interagit avec le CartesRepository pour effectuer ces opérations.
 */
@Service
public class CartesService {

    @Autowired
    private CartesRepository carteRepository;

    public void saveCartes(List<Cartes> cartes) {
        carteRepository.saveAll(cartes);
    }

    public List<Cartes> getCatesMelanger() {
        // Récupère toutes les cartes du dépôt
        List<Cartes> cartes = carteRepository.findAll();
        // réordonner les cartes
        reordonnerCartes(cartes);
        // Retourne les cartes mélangées
        return cartes;
    }

    // Méthode pour réordonner une liste de cartes
    public void reordonnerCartes(List<Cartes> list) {
        int size = list.size();
        Random rand = new Random();

        // Parcourt la liste à partir de la fin
        for (int i = size - 1; i > 0; i--) {
            // Sélectionne un index aléatoire
            int index = rand.nextInt(i + 1);
            // Échange les éléments aux positions i et index
            Cartes temp = list.get(index);
            list.set(index, list.get(i));
            list.set(i, temp);
        }
    }

    // Méthode pour obtenir toutes les cartes
    public List<Cartes> getAllCartes() {
        // Retourne toutes les cartes du dépôt
        return carteRepository.findAll();
    }
}
