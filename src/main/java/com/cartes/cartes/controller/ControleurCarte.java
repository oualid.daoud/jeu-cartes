package com.cartes.cartes.controller;


import com.cartes.cartes.model.Cartes;
import com.cartes.cartes.service.CartesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Ce contrôleur gère les requêtes HTTP liées aux cartes.
 * Il utilise l'annotation @RestController pour indiquer qu'il s'agit d'un contrôleur REST.
 * Il utilise l'annotation @RequestMapping pour mapper les requêtes web sur des méthodes de gestion spécifiques.
 *
 * @RestController est une annotation Spring qui est utilisée pour créer des services web RESTful.
 * @RequestMapping est une annotation Spring qui est utilisée pour mapper les requêtes web sur des méthodes de gestion spécifiques.
 */
@RestController
@RequestMapping("/cartes")
public class ControleurCarte {

    private final CartesService cartesService;

    /**
     * Constructeur de la classe ControleurCarte.
     * Il initialise le service des cartes.
     *
     * @param cartesService le service des cartes.
     */
    public ControleurCarte(CartesService cartesService) {
        this.cartesService = cartesService;
    }

    /**
     * Cette méthode gère les requêtes GET pour lister toutes les cartes.
     * Elle utilise l'annotation @GetMapping pour mapper les requêtes HTTP GET sur cette méthode.
     *
     * @return une réponse contenant la liste de toutes les cartes.
     */
    @GetMapping("/all")
    public ResponseEntity<List<Cartes>> AllCartes() {
        List<Cartes> cartes = cartesService.getAllCartes();
        return ResponseEntity.ok(cartes);
    }

    /**
     * Cette méthode gère les requêtes GET pour afficher toutes les cartes mélangées.
     * Elle utilise l'annotation @GetMapping pour mapper les requêtes HTTP GET sur cette méthode.
     *
     * @return une réponse contenant la liste de toutes les cartes mélangées.
     */
    @GetMapping("/cartes-aleatoires")
    public ResponseEntity<List<Cartes>> cartesAleatoires() {
        List<Cartes> cartesMelangees = cartesService.getCatesMelanger();
        return ResponseEntity.ok(cartesMelangees);
    }
}
