package com.cartes.cartes.controller;


import com.cartes.cartes.model.Cartes;
import com.cartes.cartes.service.CartesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * Contrôleur pour gérer l'affichage des cartes.
 */
@Controller
public class AffichageController {

    /**
     * Service pour gérer les opérations liées aux cartes.
     */
    @Autowired
    private CartesService cartesService;

    /**
     * Méthode pour afficher la page d'accueil.
     *
     * @return le nom de la vue "cartes".
     */
    @GetMapping("/")
    public String index() {
        return "cartes";
    }

    /**
     * Méthode pour afficher toutes les cartes.
     *
     * @param model le modèle à utiliser pour ajouter des attributs.
     * @return le nom de la vue "cartes".
     */
    @GetMapping("/afficher-cartes")
    public String cartes(Model model) {
        List<Cartes> cartes = cartesService.getAllCartes();
        model.addAttribute("cartes", cartes);
        return "cartes";
    }

}
