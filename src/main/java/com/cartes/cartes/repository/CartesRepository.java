package com.cartes.cartes.repository;

import com.cartes.cartes.model.Cartes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Ceci est l'interface du dépôt des cartes.
 * Elle étend JpaRepository pour fournir des méthodes CRUD pour l'entité Cartes.
 *
 * @Repository est une annotation Spring qui indique que la classe décorée est un dépôt.
 * Un dépôt est un mécanisme d'encapsulation du stockage, du retrait et des opérations de recherche
 * qui émet un ensemble bien défini d'interfaces pour une certaine application.
 */
@Repository
public interface CartesRepository extends JpaRepository<Cartes, Long> {
}
