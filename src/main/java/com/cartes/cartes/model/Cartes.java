package com.cartes.cartes.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

/**
 * Cette classe représente une carte dans le jeu.
 * Elle utilise les annotations Lombok pour générer les getters, les setters, le constructeur avec tous les arguments, le constructeur sans argument et la méthode toString.
 * Elle utilise également l'annotation @Entity pour indiquer qu'il s'agit d'une entité JPA.
 *
 * @AllArgsConstructor génère un constructeur avec un argument pour chaque variable d'instance dans une classe.
 * @NoArgsConstructor génère un constructeur sans argument.
 * @ToString génère une méthode toString.
 * @Entity indique qu'il s'agit d'une entité JPA.
 * @Setter génère un setter pour chaque variable d'instance dans une classe.
 * @Getter génère un getter pour chaque variable d'instance dans une classe.
 */

@NoArgsConstructor
@ToString
@Entity
@Setter
@Getter
public class Cartes {

    /**
     * Identifiant unique de la carte.
     * Il est généré automatiquement.
     */
    @jakarta.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Valeur de la carte.
     */
    private String valeur;

    /**
     * Couleur de la carte.
     */
    private String couleur;

    public Cartes(String valeur, String couleur) {
        this.valeur = valeur;
        this.couleur = couleur;
    }

}