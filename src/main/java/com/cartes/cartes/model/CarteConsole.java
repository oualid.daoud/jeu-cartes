package com.cartes.cartes.model;

import com.cartes.cartes.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Classe pour gérer l'affichage des cartes dans la console.
 */
@Setter
@Getter
@AllArgsConstructor
public class CarteConsole {

    /**
     * Logger pour enregistrer les informations.
     */
    private static final Logger logger = LogManager.getLogger(CarteConsole.class);

    /**
     * Liste des cartes à afficher.
     */
    private final List<Cartes> cartes;

    /**
     * Constructeur par défaut de CarteConsole.
     * Il initialise la liste des cartes avec toutes les combinaisons possibles de couleurs et de valeurs.
     */
    public CarteConsole() {
        cartes = Stream.of(Utils.couleurs).flatMap(couleur -> Stream.of(Utils.valeurs).map(valeur -> new Cartes(valeur, couleur))).collect(Collectors.toList());
    }

    /**
     * Cette méthode affiche toutes les cartes dans la console.
     */
    public void afficherCartes() {
        cartes.forEach(carte -> logger.info(carte));
    }
}
