package com.cartes.cartes.utils;


/**
 * Cette classe contient des constantes utilitaires pour le jeu de cartes.
 */
public class Utils {

    /**
     * Liste des valeurs possibles pour les cartes.
     */
    public static String[] valeurs = {"As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Valet", "Dame", "Roi"};

    /**
     * Liste des couleurs possibles pour les cartes.
     */
    public static String[] couleurs = {"coeur", "pique", "carreau", "trefle"};
}
